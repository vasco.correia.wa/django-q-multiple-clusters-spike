# https://docs.docker.com/compose/django/
FROM python:3.8-slim-bookworm as base

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

RUN apt update && apt install -y \
  build-essential \
  python3-dev \
  libmariadb-dev \
  default-mysql-client \
  curl \
  zip \
  unzip \
  && rm -rf /var/lib/apt/lists/*

WORKDIR /my_app

FROM base as local_dev

RUN apt update && apt install -y \
  git-crypt \
  && rm -rf /var/lib/apt/lists/*
RUN curl -sfL $(curl -s https://api.github.com/repos/powerman/dockerize/releases/latest | grep -i /dockerize-$(uname -s)-$(uname -m)\" | cut -d\" -f4) | install /dev/stdin /usr/local/bin/dockerize

COPY ./requirements.txt /my_app/requirements.txt

RUN pip install -r requirements.txt

COPY . /my_app/
