class DjangoQRouter:
    def db_for_read(self, _model, **_hints):
        if _model._meta.app_label == 'django_q':
            return 'tq_mysql'  # must match task queue's specific db connection
        if _model._meta.app_label == "django_cache":
            return "tq_mysql"
        return None

    def db_for_write(self, _model, **_hints):
        if _model._meta.app_label == 'django_q':
            return 'tq_mysql'  # must match task queue's specific db connection
        if _model._meta.app_label == "django_cache":
            return "tq_mysql"
        return None

    def allow_relation(self, _obj1, _obj2, **_hints):
        return None

    def allow_migrate(self, db, _app_label, _model_name=None, **_hints):
        if _app_label == 'django_q':
            return db == 'tq_mysql'  # must match task queue's specific db connection
        if _app_label == "django_cache":
            return "tq_mysql"
        return None
