
from collections import defaultdict
import prometheus_client
from django_prometheus.exports import ExportToDjangoView
from django_q.status import Stat
from django_q.models import OrmQ

from polls.views import QueueInfo

##########################
### Django Q + Prometheus
##########################

task_queue_clusters_gauge = prometheus_client.Gauge(
    "task_queue_clusters",
    "Clusters up in the Task Queue",
    ['queue', 'status'],
)
task_queue_workers = prometheus_client.Gauge(
    "task_queue_workers",
    "Workers in the Task Queue",
    ['queue'],
)
task_queue_tasks_in_queue = prometheus_client.Gauge(
    "task_queue_tasks_in_queue",
    "Tasks being executed or awaiting execution in the Task Queue",
    ['queue'],
)

# Custom Prometheus `/metrics` view that augments the default with Django-Q metrics
def prometheus_with_django_q(request):
    # WE UPDATE THE DJANGO Q METRICS

    queues = defaultdict(lambda: QueueInfo())

    # Example: clusters_by_queue_and_status['INSURANCE']['IDLE'] += 1
    clusters_by_queue_and_status = defaultdict(lambda: defaultdict(lambda: 0))

    for stat in Stat.get_all():
        queue_name = stat.list_key
        status = stat.status
        clusters_by_queue_and_status[queue_name][status] += 1

        queue_info = queues[stat.list_key]
        queue_info.name = stat.list_key
        queue_info.total_clusters += 1
        queue_info.total_workers += len(stat.workers)

    for queue_info in queues.values():
        queue_info.total_tasks_in_queue = OrmQ.objects.filter(key=queue_info.name).count()

    for queue_name, v in clusters_by_queue_and_status.items():
        for status, c in v.items():
            task_queue_clusters_gauge.labels(
                queue=queue_name, status=status
            ).set(c)

    for queue_info in queues.values():
        queue_name = queue_info.name
        task_queue_workers.labels(queue=queue_name).set(queue_info.total_workers)
        task_queue_tasks_in_queue.labels(queue=queue_name).set(queue_info.total_tasks_in_queue)

    # AND WE NOW USE THE "NORMAL" django_prometheus VIEW
    return ExportToDjangoView(request)
