from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('start_task/<task_type>', views.start_task, name='start_task'),
    path('stats', views.django_q_stats, name='stats'),
]
