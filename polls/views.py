from collections import defaultdict
from dataclasses import asdict, dataclass
from datetime import datetime
from random import randint

import logging
from uuid import uuid4
from django.http import HttpResponse, JsonResponse
from django.template import loader
from django_q.status import Stat
from django_q.tasks import async_task
from django_q.humanhash import humanize
from django_q.models import OrmQ

_logger = logging.getLogger(__name__)

def index(request):
    # return HttpResponse("Hello, world. You're at the polls index.")
    template = loader.get_template('polls/index.html')
    return HttpResponse(template.render({}, request))


def start_task(request, task_type: str):
    task_type = task_type.strip().upper()

    print(f'Submitted task of type {task_type}')

    name = uuid4()
    sleep_time = randint(10, 40)

    if task_type == 'INSURANCE':
        async_task(_dummy_task_that_takes_a_bit, name, task_type, sleep_time, q_options={'cluster': task_type})
        print(f"Created task in INSURANCE - {name} - {sleep_time}")
    elif task_type == 'SHIPPING':
        async_task(_dummy_task_that_takes_a_bit, name, task_type, sleep_time)
        print(f"Created task in SHIPPING - {name} - {sleep_time}")
    else:
        async_task(_dummy_task_that_takes_a_bit, name, task_type, sleep_time)
        print(f"Created task in DEFAULT - {name} - {sleep_time}")

    return HttpResponse()



def _dummy_task_that_takes_a_bit(name, expected_queue, sleep_time):
    print(f"Started task -- {expected_queue} -- {name} -- {sleep_time}")
    # sleep is not OK because it makes the thread "lose priority"
    # time.sleep(sleep_time)
    # active loop is best to ensure this actually blocks the TQ
    for _ in range(sleep_time * 10000000):
        pass
    print(f"Finished task -- {expected_queue} -- {name} -- {sleep_time}")



#################################
### STUFF about Django-Q metrics
#################################

@dataclass
class QueueInfo:
    name: str = ''
    total_clusters: int = 0
    total_workers: int = 0
    total_tasks_in_queue: int = 0

# Django view that reports Django-Q metrics
def django_q_stats(request):

    total_clusters = 0
    clusters_in_status = defaultdict(lambda: 0)
    clusters = {}
    queues = defaultdict(lambda: QueueInfo())

    for stat in Stat.get_all():
        total_clusters += 1
        clusters_in_status[str(stat.status)] += 1

        clusters[humanize(stat.cluster_id.hex)] = {
            "name": humanize(stat.cluster_id.hex),
            "cluster_id": stat.cluster_id,
            "queue_name": stat.list_key,
            "host": stat.host,
            "status": stat.status,
            "uptime": datetime.fromtimestamp(stat.uptime()),
            "reincarnations": stat.reincarnations,
            "task_q_size": stat.task_q_size,
            "done_q_size": stat.done_q_size,
            "workers": stat.workers,
        }
        queue_info = queues[stat.list_key]
        queue_info.name = stat.list_key
        queue_info.total_clusters += 1
        queue_info.total_workers += len(stat.workers)

    for queue_info in queues.values():
        queue_info.total_tasks_in_queue = OrmQ.objects.filter(key=queue_info.name).count()

    serialized_queue_info = {k: asdict(v) for k, v in queues.items()}

    data = {
        "total": total_clusters,
        "clusters_in_status": clusters_in_status,
        "clusters": clusters,
        "queues": serialized_queue_info,
    }

    return JsonResponse(data)
